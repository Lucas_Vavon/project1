package com.example.demo;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;


public class DemoApplicationTests {


	@Test
	public void testSomeMethod(){
		int[] actual = DemoApplication.superSort(new int[]{-3,-2,-1,0,1});
		assertArrayEquals(new int[]{-3,-2,-1,0,1}, actual);
	}	
}
