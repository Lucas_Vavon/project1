package com.example.demo;


public class DemoApplication {

	public static int[] superSort(int[] arrayToSort){

		boolean sorted = false;

		while (!sorted) {
			sorted = true;
			for(int i = 0 ; i < (arrayToSort.length-1) ; i++){

				if (arrayToSort[i] > arrayToSort [i+1]) {
				int tmp = arrayToSort[i];
				arrayToSort[i] = arrayToSort[i+1];
				arrayToSort[i+1] = tmp;
				sorted = false;
				}

			}
		}
		return arrayToSort;
	}

}